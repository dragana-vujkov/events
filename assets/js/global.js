function resetForm($form)
{
    $form.find('input:text, input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}

$(document).ready(function(){

    // add
    $("#add-modal input[name='date_start']").datepicker({format: 'yyyy-mm-dd'});
    $("#add-modal input[name='date_end']").datepicker({format: 'yyyy-mm-dd'});

    $('#add-modal .btn-primary').click(function() {
        $.ajax({
            url: root + "/events/add",
            type : 'POST',
            data: $('#add-form').serializeArray(),
            success: function(response)
            {
                $('#add-modal').modal('hide');
                resetForm($('#add-form'));
                window.location.href = root;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    });

    // edit
    $("#edit-modal input[name='date_start']").datepicker({format: 'yyyy-mm-dd'});
    $("#edit-modal input[name='date_end']").datepicker({format: 'yyyy-mm-dd'});

    $('.edit').click(function() {
        id = $(this).data("id");
        //alert(id);
        $.ajax({
            url: root + "/events/read",
            type : 'POST',
            data: {'id' : id},
            success: function(response)
            {
                $("#edit-modal #edit-id").val(id);
                $("#edit-modal input[name='title']").val(response.title);
                $("#edit-modal textarea[name='description']").val(response.description);
                $("#edit-modal input[name='date_start']").val(response.date_start);
                $("#edit-modal input[name='date_end']").val(response.date_end);
                $('#edit-modal').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    });

    $('#edit-modal .btn-primary').click(function() {
        $.ajax({
            url: root + "/events/update",
            type : 'POST',
            data: $('#edit-form').serializeArray(),
            success: function(response)
            {
                window.location.href = root;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    });


    // delete
    $('.delete').click(function() {
        id = $(this).data("id");
        $("#delete-modal #delete-id").val(id);
    });

    $('#delete-modal .btn-primary').click(function() {
        $.ajax({
            url: root + "/events/delete",
            type : 'POST',
            data: {'id' : $("#delete-modal #delete-id").val()},
            success: function(response)
            {
                window.location.href = root;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    });

});