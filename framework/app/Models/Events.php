<?php namespace App\Models;

use Eloquent;

class Events extends Eloquent
{
    public $timestamps = FALSE;
    protected $messagebag;
    protected $table = 'events'; // The database table used by the model.
}