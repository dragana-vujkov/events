<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// get
    Route::get('/', 'EventController@getList');
// post
    Route::post('/events/add', 'EventController@postAdd');
    Route::post('/events/read', 'EventController@postRead');
    Route::post('/events/update', 'EventController@postUpdate');
    Route::post('/events/delete', 'EventController@postDelete');