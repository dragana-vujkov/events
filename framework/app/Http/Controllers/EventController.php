<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Events;

use Request;
use Response;
use Auth;
use View;
use Form;
use URL;
use DB;
use Input;
use Session;
use Redirect;
use Log;
use Config;


class EventController extends BaseController
{
   
    function __construct()
    {
        parent::__construct();
    }

    public function getList()
    {
        $events = Events::orderBy('date_start', 'desc')->paginate(10);

        return View::make('events', ['events' => $events]);
    }

    public function postAdd()
    {
        $event = new Events;
        $event->title = Input::get('title');
        $event->description = Input::get('description');
        $event->date_start = Input::get('date_start');
        $event->date_end = Input::get('date_end');
        $event->save();
    }

    public function postRead()
    {
        $event = Events::find(Input::get('id'));

        return Response::json([
            'title' => $event->title,
            'description' => $event->description,
            'date_start' => $event->date_start,
            'date_end' => $event->date_end,
        ]);
    }

    public function postUpdate()
    {
        $event = Events::find(Input::get('id'));
        $event->title = Input::get('title');
        $event->description = Input::get('description');
        $event->date_start = Input::get('date_start');
        $event->date_end = Input::get('date_end');
        $event->save();
    }

    public function postDelete()
    {
        $event = Events::find(Input::get('id'));
        $event->delete();
    }

}