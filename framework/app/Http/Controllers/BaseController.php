<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App;
use Auth;
use DB;
use View;
use Input;
use Helpers;
use Session;
use Route;
use Redirect;


class BaseController extends Controller {

    public function __construct()
    {
        DB::enableQueryLog();
    }

}

