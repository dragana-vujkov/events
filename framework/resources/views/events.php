<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Events</title>
    <link type="text/css" rel="stylesheet" href="<?php echo URL::to('/'); ?>/assets/css/bootstrap.min.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo URL::to('/'); ?>/assets/css/bootstrap-theme.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo URL::to('/'); ?>/assets/css/bootstrap-datepicker.min.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo URL::to('/'); ?>/assets/css/global.css" media="screen" />
    <script type="text/javascript" src="<?php echo URL::to('/'); ?>/assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo URL::to('/'); ?>/assets/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo URL::to('/'); ?>/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo URL::to('/'); ?>/assets/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo URL::to('/'); ?>/assets/js/global.js"></script>
    <script type="text/javascript" charset="utf-8">
        var root = '<?php echo URL::to('/'); ?>';
    </script>
</head>
<body>

    <div class="modal fade" id="add-modal" tabindex='-1'>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Add</h4>
                </div>
                <div class="modal-body">
                    <form role="form" id="add-form">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" maxlength="250">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" maxlength="250"></textarea>
                            <label for="date_start">Date Start</label>
                            <input type="text" class="form-control" name="date_start" maxlength="10">
                            <label for="date_end">Date End</label>
                            <input type="text" class="form-control" name="date_end" maxlength="10">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Add</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="edit-modal" tabindex='-1'>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Edit</h4>
                </div>
                <div class="modal-body">
                    <form role="form" id="edit-form">
                        <div class="form-group">
                            <input type="hidden" name="id" id="edit-id">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" maxlength="250">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" maxlength="250"></textarea>
                            <label for="date_start">Date Start</label>
                            <input type="text" class="form-control" name="date_start" maxlength="10">
                            <label for="date_end">Date End</label>
                            <input type="text" class="form-control" name="date_end" maxlength="10">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="delete-modal" tabindex='-1'>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Delete</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="delete-id" id="delete-id">
                    Delete selected event?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Delete</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo URL::to('/'); ?>">Events</a>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="content">
            <div class="row controls">
                <button class="btn btn-primary" data-target="#add-modal" data-toggle="modal" data-keyboard="true" type="button">Add</button>
            </div>
            <div class="row">
                <h1>Events</h1>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Event</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $counter = $events->firstItem();
                    //$counter = 1;
                    foreach ($events as $event)
                    {
                    ?>
                    <tr>
                        <td><?php echo $counter; ?>.</td>
                        <td>
                            <?php
                                echo '<span class="title">' . $event->title . '</span>';
                                echo '<span class="period"><span>from <span>' . $event->date_start . '</span> to <span>' . $event->date_end . '</span></span></span>';
                                echo '<span class="description">' . $event->description . '</span>';
                            ?>
                        </td>
                        <td>
                            <button class="btn btn-sm btn-default edit" data-toggle="modal" data-keyboard="true" data-id="<?php echo $event->id; ?>" type="button">Edit</button>
                            <button class="btn btn-sm btn-default delete" data-target="#delete-modal" data-toggle="modal" data-keyboard="true" data-id="<?php echo $event->id; ?>" type="button">Delete</button>
                        </td>
                    </tr>
                    <?php
                        $counter++;
                    }
                    ?>
                    </tbody>
                </table>

                <div class="pagination_wrapper">
                    <?php echo $events->appends([])->render(); ?>
                    <div class="pagination_detail">
                        <?php echo $events->total(); ?> total items.
                        Page <?php echo $events->currentPage(); ?> of <?php echo $events->lastPage(); ?>.
                    </div>
                </div>

            </div>
        </div>
    </div><!-- /.container -->

</body>
</html>